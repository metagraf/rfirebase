# R Firebase API

R wrapper for the [Firebase's REST API](https://www.firebase.com/docs/rest-api.html).

## Installation

```
require(devtools)
install_github("rFirebase", "metagraf")
```
